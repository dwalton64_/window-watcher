# The MIT License (MIT)
#
# Copyright (c) 2014 Andrew C. Walton
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import httplib
import json
import traceback
import urllib2
import secrets

__author__ = 'Andrew'


class NestThermostat():
    def __init__(self, ambient_temperature_f, device_id, hvac_mode, is_online, last_connection, name, software_version,
                 structure_id, target_temperature_f, target_temperature_high_f, target_temperature_low_f):
        self.ambient_temperature_f = ambient_temperature_f
        self.device_id = device_id
        self.hvac_mode = hvac_mode
        self.is_online = is_online
        self.last_connection = last_connection
        self.name = name
        self.software_version = software_version
        self.structure_id = structure_id
        self.target_temperature_f = target_temperature_f
        self.target_temperature_high_f = target_temperature_high_f
        self.target_temperature_low_f = target_temperature_low_f

    def print_current_temp_f(self):
        print("Thermostat " + self.name + " measures a current temperature of " + str(
            self.ambient_temperature_f) + " degrees F")

    def print_target_temp_f(self):
        print("Thermostat " + self.name + " is set to " + str(self.target_temperature_f) + " degrees F")


class NestStructure():
    def __init__(self, away, name, structure_id, thermostat_list):
        self.away = away
        self.name = name
        self.structure_id = structure_id
        self.thermostats = thermostat_list


class Nest():
    def __init__(self):
        # Read Authorization Token for NEST API
        auth_token = secrets.auth_token
        self.nestUserUrl = "https://developer-api.nest.com/?auth=" + auth_token
        self.thermostats = None
        self.nest_structures = None
        self.nest_data_available = None
        self.error_message = ""
        self.update()

    def update(self):
        try:
            nest_user_file = urllib2.urlopen(self.nestUserUrl)
            nest_user_json = json.loads(nest_user_file.read())

            # copy thermostat data out of the json from Nest
            thermostat_data = []
            for tstat_id in nest_user_json['devices']['thermostats']:
                thermostat_data.append(nest_user_json['devices']['thermostats'][tstat_id])

            # create thermostat objects containing the thermostat data we want to access
            self.thermostats = []
            for tstat in thermostat_data:
                self.thermostats.append(
                    NestThermostat(tstat['ambient_temperature_f'], tstat['device_id'], tstat['hvac_mode'],
                                   tstat['is_online'], tstat['last_connection'], tstat['name'],
                                   tstat['software_version'], tstat['structure_id'], tstat['target_temperature_f'],
                                   tstat['target_temperature_high_f'], tstat['target_temperature_low_f']))

            # put the structure (building/house) data in the json from Nest into an object
            self.nest_structures = []
            for nstructs_id in nest_user_json['structures']:
                self.nest_structures.append(NestStructure(nest_user_json['structures'][nstructs_id]['away'],
                                                          nest_user_json['structures'][nstructs_id]['name'],
                                                          nest_user_json['structures'][nstructs_id]['structure_id'],
                                                          nest_user_json['structures'][nstructs_id]['thermostats']))

            # pprint(nest_user_json)
            nest_user_file.close()
            self.nest_data_available = True

        # If accessing the URL fails, we can use old values for a few polls
        except urllib2.HTTPError, e:
            self.error_message = self.nestUserUrl + ' had HTTPError = ' + str(e.code)
            self.error_message += traceback.format_exc()
            print(self.error_message)
            self.invalidate_data()
        except urllib2.URLError, e:
            self.error_message = self.nestUserUrl + ' had URLError = ' + str(e.reason)
            self.error_message += traceback.format_exc()
            print(self.error_message)
            self.invalidate_data()
        except httplib.HTTPException, e:
            self.error_message = self.nestUserUrl + ' had HTTPException'
            print(self.error_message)
            print(e)
            self.invalidate_data()

    def invalidate_data(self):
        self.nest_data_available = False
        self.thermostats = []
        self.nest_structures = []

    def list_structure_names(self):
        names = []
        for structure in self.nest_structures:
            names.append(structure.name)

        return names
