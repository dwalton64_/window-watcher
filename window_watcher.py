#!/usr/bin/env python
# The MIT License (MIT)
#
# Copyright (c) 2014 Andrew C. Walton
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import time
import smtplib
from email.mime.text import MIMEText
import sys
import urllib2

# API keys, user names and passwords are kept in a separate file so that they will not be checked into git.
import secrets  # copy secrets_template.py to secrets.py and put your own secrets into it

from airnow import AirQuality
from nest import Nest
from weather_underground import Weather


def send_gmail(gmail_username, gmail_password, email_subject, recipient_list, email_body):
    global COMMASPACE, msg, session
    COMMASPACE = ', '
    msg = MIMEText(email_body)
    msg['Subject'] = email_subject
    msg['From'] = gmail_username
    msg['To'] = COMMASPACE.join(recipient_list)
    session = smtplib.SMTP('smtp.gmail.com', 587)
    session.ehlo()
    session.starttls()
    session.login(gmail_username, gmail_password)
    session.sendmail(gmail_username, recipient_list, msg.as_string())
    session.quit()


def open_website_to_get_nest_pin():
    """
    This function opens a web page on Nest.com that will get you the pin authorization code necessary to get an
    authToken.

    This is the URL to which your users go to enable access to your application.  They log into their Nest account
    and Nest sees the clientID that it gave you when you registered as a developer.  The Nest website will display
    a page with your pin authorization code on it.  The user gives you the pin from the web site and you use the pin
    authorization code to request an authorization token that you will include in all of your API accesses to Nest.

    """
    import webbrowser

    webbrowser.open("https://home.nest.com/login/oauth2?client_id=" + secrets.clientID + "&state=STATE")


def get_auth_token_from_nest():
    authorization_code = raw_input("Enter the authorization pin code you got from Nest.com: ")
    nest_token_url = "https://api.home.nest.com/oauth2/access_token?code=" + authorization_code + "&client_id=" + \
                     secrets.clientID + "&client_secret=" + secrets.clientSecret + "&grant_type=authorization_code"
    authorization_token = urllib2.urlopen(nest_token_url)
    print("Copy the data below to auth_token in secrets.py:")
    print(authorization_token)

# FIXME - get pollen count from www.claritin.com (only once a day)
# http://www.claritin.com/weatherpollenservice/weatherpollenservice.svc/getforecast/95765

# #####################################################################################################################
myWeather = Weather(secrets.state, secrets.city)
myAirQuality = AirQuality(secrets.zipcode, 25)
myNest = Nest()

# FIXME add a for loop for each structure and give advice for each structure?
# Look the structure up by its name.  I will need to add the name of the structure to secrets.py
try:
    print("Starting the main loop ...")
    previous_window_state = []
    while True:
        data_good = True
        body_of_email = ""
        if not myWeather.weather_available:
            body_of_email += "There was an error getting the weather data from Weather Underground.\n"
            data_good = False

        if not myAirQuality.air_quality_available:
            body_of_email += "There was an error getting air quality data from AirNow.gov.\n"
            data_good = False

        if not myNest.nest_data_available:
            body_of_email += "There was an error getting thermostat data from nest.com.\n"
            body_of_email += myNest.error_message
            data_good = False

        if not data_good:
            username = secrets.gmail_user
            password = secrets.gmail_password
            recipients = secrets.error_recipients
            subject = "The weather watcher had an error getting weather data . . . "
            send_gmail(username, password, subject, recipients, body_of_email)
        else:
            open_windows = False
            body_of_email = str(myWeather)
            body_of_email += "\n"

            for thermostat in myNest.thermostats:
                body_of_email += "For the thermostat " + thermostat.name + ":\n"
                if thermostat.hvac_mode == 'cool':
                    if myWeather.temp_f < thermostat.target_temperature_f:
                        open_windows = True
                        body_of_email += " - It is cool outside (" + str(myWeather.temp_f) + " Deg F). \n"
                    else:
                        body_of_email += " - It is too hot outside to have the windows open. \n"
                    body_of_email += " - The thermostat is set at " + str(thermostat.target_temperature_f) + " Deg F\n"

                if thermostat.hvac_mode == 'heat-cool':
                    if thermostat.target_temperature_high_f > myWeather.temp_f > thermostat.target_temperature_low_f:
                        open_windows = True
                        body_of_email += " - The thermostat is set to heat-cool and it is comfortable outside (" + \
                                         str(myWeather.temp_f) + " Deg F). \n"
                    elif myWeather.temp_f >= thermostat.target_temperature_high_f:
                        body_of_email += " - The thermostat is set to heat-cool and it is hot outside (" + \
                                         str(myWeather.temp_f) + " Deg F). \n"
                    else:
                        body_of_email += " - The thermostat is set to heat-cool and it is cold outside (" + \
                                         str(myWeather.temp_f) + " Deg F). \n"
                    body_of_email += " - The thermostat is set to cool at " + \
                                     str(thermostat.target_temperature_high_f) + " Deg F\n"
                    body_of_email += " - The thermostat is set to heat at " + \
                                     str(thermostat.target_temperature_low_f) + " Deg F\n"

                if thermostat.hvac_mode == 'heat':
                    if myWeather.temp_f > thermostat.target_temperature_f:
                        open_windows = True
                        body_of_email += " - The thermostat is set to heat and it is warm outside (" + \
                                         str(myWeather.temp_f) + " Deg F). \n"
                    else:
                        body_of_email += " - The thermostat is set to heat and it is cool outside (" + \
                                         str(myWeather.temp_f) + " Deg F). \n"
                    body_of_email += " - The thermostat is set at " + str(thermostat.target_temperature_f) + " Deg F\n"

                body_of_email += "\n"

            body_of_email += str(myAirQuality)
            if myAirQuality.is_air_quality_good() is False:
                open_windows = False
                body_of_email += "The air quality is bad, you don't want to open the windows.\n"
            else:
                body_of_email += "The air quality is good.\n"

            if open_windows is True:
                subject = 'It is nice out.  You should open the windows.'
                body_of_email += "It is nice outside.  You should open the windows!!\n"

            else:
                subject = 'The weather is not good.  Close the windows.'

            print("SUBJECT:  " + subject)
            print(body_of_email)

            if open_windows != previous_window_state:
                print("The recommended window state has changed. Sending an email.")
                username = secrets.gmail_user
                password = secrets.gmail_password
                recipients = secrets.recipients

                send_gmail(username, password, subject, recipients, body_of_email)
            else:
                print("The recommended window state has not changed. No email sent.")
            print("\n\n")

            sys.stdout.flush()  # flush buffers so that we can see output when it is piped to a file
            previous_window_state = open_windows

        time.sleep(30.0 * 60.0)  # wait for 30 minutes so we don't abuse our access to web services

        # get updated data
        myWeather.update_weather()
        myAirQuality.update_air_quality()
        myNest.update()

except KeyboardInterrupt:
    print('\n\nKeyboard exception received. Exiting.')
    exit()
