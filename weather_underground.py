#!/usr/bin/env python
# The MIT License (MIT)
#
# Copyright (c) 2014 Andrew C. Walton
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import httplib
import json
from pprint import pprint
import sys
import traceback
import urllib2
import secrets

__author__ = 'Andrew'


class Weather():
    def __init__(self, state, city):
        self.wu_url = 'http://api.wunderground.com/api/' + secrets.wu_api_key + '/geolookup/conditions/q/' + state + \
                      '/' + city + '.json'
        self.json_string = None
        self.location = None
        self.temp_f = None
        self.wind = None
        self.precip = None
        self.humidity = None
        self.observation_time = None
        self.weather_available = False
        self.update_weather()

    def update_weather(self):
        # Get Weather from Weather Underground
        try:
            wunder_file = urllib2.urlopen(self.wu_url)

            self.json_string = wunder_file.read()
            wu_parsed_json = json.loads(self.json_string)
            self.location = wu_parsed_json['location']['city']
            self.temp_f = wu_parsed_json['current_observation']['temp_f']
            self.wind = wu_parsed_json['current_observation']['wind_string']
            self.precip = wu_parsed_json['current_observation']['precip_today_in']
            self.humidity = wu_parsed_json['current_observation']['relative_humidity']
            self.observation_time = wu_parsed_json['current_observation']['observation_time']

            wunder_file.close()
            self.weather_available = True

        # If accessing the URL fails, we can use old values for a few polls
        except urllib2.HTTPError, e:
            print(self.wu_url + ' had HTTPError = ' + str(e.code))
            self.weather_available = False
        except urllib2.URLError, e:
            print(self.wu_url + ' had URLError = ' + str(e.reason))
            self.weather_available = False
            traceback.print_exc(file=sys.stdout)
        except httplib.HTTPException, e:
            print(self.wu_url + ' had HTTPException')
            print(e)
            self.weather_available = False

    def print_weather(self):
        print(str(self))

    def print_weather_json(self):
        pprint(self.json_string)

    def __str__(self):
        if self.weather_available:
            string = "Current temperature in %s is: %s Deg F\n" % (self.location, self.temp_f)
            string += "Wind: %s\n" % self.wind
            string += "Inches of precipitation today: %s \n" % self.precip
            string += "Relative humidity: %s\n" % self.humidity
            string += "Weather %s\n" % self.observation_time
        else:
            string = "Most recent call to Weather Underground API Failed. No weather data available."
        return string


# FIXME use http://api.wunderground.com/api/Your_Key/geolookup/q/94107.json to lookup state and city
def lookup_state_and_city():
    url = 'http://api.wunderground.com/api/' + secrets.wu_api_key + '/geolookup/q/' + \
          str(secrets.zipcode) + '.json'

    wunder_file = urllib2.urlopen(url)
    json_string = wunder_file.read()
    wu_parsed_json = json.loads(json_string)
    pprint(wu_parsed_json)
    city = wu_parsed_json['location']['city']
    state = wu_parsed_json['location']['state']
    print('Weather underground mapped zipcode ' + str(secrets.zipcode) +  ' to ' + city + ', ' + state)