# Window Watcher #

Window Watcher is a simple, open-source program that uses weather data, air quality data and information from a Nest thermostat to decide whether the user should open or close the windows and sends an email to the user.

### How do I get set up? ###

* Summary of set up
This application accesses APIs for Weather Underground, Airnow and Nest and you will need to register developer accounts to get access to these APIs.  Copy the file secrets_template.py to secrets.py and add the API keys, authorization token and email account information into the file so that the application can access the APIs and send email.

* Configuration
This has been tested with python 2.7.6 in Mint 17, Windows 8.1 and Rasbian Linux on a Raspberry Pi.

* Dependencies
Uses modules from the standard library.

* Database configuration
None


### Who do I talk to? ###

* dwalton64 at gmail dot com
