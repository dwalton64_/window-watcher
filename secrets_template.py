# The MIT License (MIT)
#
# Copyright (c) 2014 Andrew C. Walton
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# This file is used to keep things that should not be checked into version control software such as API keys and
# passwords.
#
# This file should be copied to secrets.py and then the appropriate lines filled in,

#################################################################
# Weather Underground secrets
#
# Get your own key at:
#  http://www.wunderground.com/weather/api
#
wu_api_key = 'your_key_goes_here'

#################################################################
# Airnow secrets
#
# Get your own key at:
#   http://www.airnowapi.org/
#
airnow_api_key = 'your_key_goes_here'

#################################################################
# Nest secrets
#
# Register your Nest app at:
#   https://developer.nest.com/
#
# The clientID is inserted into an Authorization URL that is used by the owner of the Nest devices to log into the Nest
# website and see what permissions your application needs.  The Nest website will give them a pin that you can use to
# get an authorization token that allows your application to access data from their devices.
#
# Once you have registered your Nest application, you can get the clientID from:
#   https://developer.nest.com/clients
#
clientID = "your_client_id_goes_here"

# Once the Nest customer has a pin for your app, you need to use the pin, along with your clientSecret to get an
# authorization token.  The client secret is also available at:
#   https://developer.nest.com/clients
#
clientSecret = "your_client_secret_goes_here"

# Nest Authorization Token
auth_token = 'copy the token you get from Nest into this file'


#################################################################
# Gmail account data
gmail_user = "sender@isp.com"
gmail_password = "senders password goes here"

# The recipients listed below get email when they should open or close the windows.
recipients = ["recipient1@isp.com", "recipient2@isp.com"]

# The recipients listed below get email when there is an error accessing web APIs.
error_recipients = ["recipient1@isp.com", "recipient2@isp.com"]


#################################################################
# Location data - put your location into the fields below
zipcode = 87123
State = "NM"
City = "Albuquerque"