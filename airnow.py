# The MIT License (MIT)
#
# Copyright (c) 2014 Andrew C. Walton
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
import httplib
import json
import sys
import traceback
import urllib2
import secrets

__author__ = 'Andrew'


class AqiParam():
    def __init__(self, aqi_value, param_name, aqi_state, reporting_area, state_code):
        self.aqi_value = aqi_value
        self.param_name = param_name
        self.aqi_state = aqi_state
        self.reporting_area = reporting_area
        self.state_code = state_code

    def __str__(self):
        string = "%s: %s (%s) measured at %s, %s\n" % (self.param_name, self.aqi_value, self.aqi_state,
                                                       self.reporting_area, self.state_code)
        return string


class AirQuality():
    def __init__(self, zipcode, distance_mi):
        """
        AirQuality is an air quality object that gets it's data using an API call to the Air Now website.
        :param zipcode: The zipcode for which you want air quality data.
        :param distance_mi: Radius from the zip code in miles to search for a Air Now monitoring station.
        """
        self.airnow_url = 'http://www.airnowapi.org/aq/observation/zipCode/current/' + \
                          '?format=application/json&zipCode=' + \
                          str(zipcode) + '&distance=' + str(distance_mi) + \
                          '&API_KEY=' + secrets.airnow_api_key
        self.json_string = None
        self.aqi_param_list = None
        self.air_quality_available = None
        self.update_air_quality()

    def update_air_quality(self):
        """
        Makes an API call to AirNow and updates this object's list of air quality parameters.

        Note that the number of parameters returned depends on the Air Now monitoring site and the state of the site.
        For example, a particular site may support returning ozone and 2.5 micron particulate matter but may only
        return ozone for a brief period of time while it is updating the particulate matter parameter.

        """
        try:
            airnow_file = urllib2.urlopen(self.airnow_url)
            self.json_string = airnow_file.read()
            airnow_json = json.loads(self.json_string)
            self.aqi_param_list = []
            for param in airnow_json:
                self.aqi_param_list.append(AqiParam(param['AQI'], param['ParameterName'], param['Category']['Name'],
                                                    param['ReportingArea'], param['StateCode']))
            airnow_file.close()
            self.air_quality_available = True

        # If accessing the URL fails, we can use old values for a few polls
        except urllib2.HTTPError, e:
            print(self.airnow_url + ' had HTTPError = ' + str(e.code))
            self.air_quality_available = False
        except urllib2.URLError, e:
            print(self.airnow_url + ' had URLError = ' + str(e.reason))
            self.air_quality_available = False
            traceback.print_exc(file=sys.stdout)
        except httplib.HTTPException, e:
            print(self.airnow_url + ' had HTTPException')
            print(e)
            self.air_quality_available = False

    def print_params(self):
        """
        Prints all of the air quality parameters to the console.

        AirNow provides one or more parameters and values for the parameters for each monitoring station.  It also
        provides a interpretation of the parameter such as "good," "moderate," or "unhealthy."

        """
        print(str(self))

    def __str__(self):
        string = ""
        if self.air_quality_available:
            # Airnow returned one or more parameters from the API call.  Iterate through them and print each one.
            for param in self.aqi_param_list:
                string += str(param)
            return string
        else:
            string = "Most recent call to AirNow API Failed. No air quality data available."
        return string

    def is_air_quality_good(self):
        """
        Checks all of the air quality parameters returned from Air Now to determine if the air quality is good.

        :return:

        True  - if all of the parameters are Good (indicating that air quality is good)
        False - if one or more of the parameters indicate that the air quality is moderate or worse.
                (indicating air quality is bad)
        """
        good = True
        for param in self.aqi_param_list:
            if param.aqi_state != 'Good':
                good = False
        return good